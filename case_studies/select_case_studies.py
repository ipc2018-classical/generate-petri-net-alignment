#!/usr/bin/env python

import os

import random

random.seed(10)
def chunks_random_selection(l, n):
    chunks = [l[i:i + n] for i in range(0, len(l), len(l)//n)]

    return [random.choice(l) for l in chunks]

def get_num_events(path, p):
    f = open ("{}/{}".format(path, p))
    events = 0
    for l in f.readlines():
        if "- event" in l:
            events += 1
    f.close()
    return events

    
for d in os.listdir('.'):
    if os.path.isdir(d) and d != 'final_selection':
        for domain in os.listdir('./{}'.format(d)):
            path = './{}/{}'.format(d, domain)
            if os.path.isdir(path):
                problem_list = sorted ([(get_num_events(path, p), p) for p in os.listdir(path) if p.startswith('p')])
                reduced_problem_list = chunks_random_selection (problem_list, 10)
                for p in reduced_problem_list:
                    print ("cp {}/{} final_selection/domain-{}-{}-{}".format(path, p[1].replace('problem', 'domain'), domain, p[0], p[1].replace('problem', '')))
                    print ("cp {}/{} final_selection/{}-{}-{}".format(path, p[1], domain, p[0], p[1].replace('problem', '')))

                

                        
