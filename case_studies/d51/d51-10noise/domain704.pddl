(define (domain Mining)
(:requirements :typing :equality)
(:types place event)

(:predicates
(token ?p - place)
(tracePointer ?e - event)
(allowed)
)

(:functions
(total-cost)
)

(:action moveSync#activitybl#ev19
:precondition (and (token 1178) (tracePointer ev19))
:effect (and (allowed) (not (token 1178)) (token 1188) (not (tracePointer ev19)) (tracePointer ev20))
)

(:action moveInTheModel#activitybl
:precondition (token 1178)
:effect (and (not (allowed)) (not (token 1178)) (token 1188) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityan
:precondition (token 1168)
:effect (and (not (allowed)) (not (token 1168)) (token 1222) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv0
:precondition (and (token 1224) (token 1226) (token 1228))
:effect (and (not (allowed)) (not (token 1224)) (not (token 1226)) (not (token 1228)) (token 1230) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activitycl
:precondition (token 1186)
:effect (and (not (allowed)) (not (token 1186)) (token 1160) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybj
:precondition (token 1242)
:effect (and (not (allowed)) (not (token 1242)) (token 1241) (increase (total-cost) 1)
)
)

(:action moveSync#activitycd#ev17
:precondition (and (token 1211) (tracePointer ev17))
:effect (and (allowed) (not (token 1211)) (token 1186) (not (tracePointer ev17)) (tracePointer ev18))
)

(:action moveInTheModel#activitycd
:precondition (token 1211)
:effect (and (not (allowed)) (not (token 1211)) (token 1186) (increase (total-cost) 1)
)
)

(:action moveSync#activityc#ev3
:precondition (and (token 1150) (tracePointer ev3))
:effect (and (allowed) (not (token 1150)) (token 1192) (not (tracePointer ev3)) (tracePointer ev4))
)

(:action moveInTheModel#activityc
:precondition (token 1150)
:effect (and (not (allowed)) (not (token 1150)) (token 1192) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv1
:precondition (and (token 1258) (token 1256) (token 1261))
:effect (and (not (allowed)) (not (token 1258)) (not (token 1256)) (not (token 1261)) (token 1263) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activityw
:precondition (token 1215)
:effect (and (not (allowed)) (not (token 1215)) (token 1218) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycq
:precondition (token 1203)
:effect (and (not (allowed)) (not (token 1203)) (token 1204) (increase (total-cost) 1)
)
)

(:action moveSync#activityab#ev9
:precondition (and (token 1154) (tracePointer ev9))
:effect (and (allowed) (not (token 1154)) (token 1155) (not (tracePointer ev9)) (tracePointer ev10))
)

(:action moveInTheModel#activityab
:precondition (token 1154)
:effect (and (not (allowed)) (not (token 1154)) (token 1155) (increase (total-cost) 1)
)
)

(:action moveSync#activitycg#ev31
:precondition (and (token 1199) (tracePointer ev31))
:effect (and (allowed) (not (token 1199)) (token 1187) (not (tracePointer ev31)) (tracePointer ev32))
)

(:action moveInTheModel#activitycg
:precondition (token 1199)
:effect (and (not (allowed)) (not (token 1199)) (token 1187) (increase (total-cost) 1)
)
)

(:action moveSync#activityah#ev15
:precondition (and (token 1239) (tracePointer ev15))
:effect (and (allowed) (not (token 1239)) (token 1168) (not (tracePointer ev15)) (tracePointer ev16))
)

(:action moveInTheModel#activityah
:precondition (token 1239)
:effect (and (not (allowed)) (not (token 1239)) (token 1168) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityq
:precondition (token 1210)
:effect (and (not (allowed)) (not (token 1210)) (token 1175) (increase (total-cost) 1)
)
)

(:action moveSync#activityak#ev32
:precondition (and (token 1172) (tracePointer ev32))
:effect (and (allowed) (not (token 1172)) (token 1169) (not (tracePointer ev32)) (tracePointer ev33))
)

(:action moveInTheModel#activityak
:precondition (token 1172)
:effect (and (not (allowed)) (not (token 1172)) (token 1169) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybf
:precondition (token 1178)
:effect (and (not (allowed)) (not (token 1178)) (token 1180) (increase (total-cost) 1)
)
)

(:action moveSync#activityay#ev40
:precondition (and (token 1236) (tracePointer ev40))
:effect (and (allowed) (not (token 1236)) (token 1157) (not (tracePointer ev40)) (tracePointer ev41))
)

(:action moveInTheModel#activityay
:precondition (token 1236)
:effect (and (not (allowed)) (not (token 1236)) (token 1157) (increase (total-cost) 1)
)
)

(:action moveSync#activityci#ev28
:precondition (and (token 1189) (tracePointer ev28))
:effect (and (allowed) (not (token 1189)) (token 1191) (not (tracePointer ev28)) (tracePointer ev29))
)

(:action moveInTheModel#activityci
:precondition (token 1189)
:effect (and (not (allowed)) (not (token 1189)) (token 1191) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityg
:precondition (token 1150)
:effect (and (not (allowed)) (not (token 1150)) (token 1152) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycr
:precondition (token 1205)
:effect (and (not (allowed)) (not (token 1205)) (token 1207) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityac
:precondition (token 1155)
:effect (and (not (allowed)) (not (token 1155)) (token 1154) (increase (total-cost) 1)
)
)

(:action moveSync#activityba#ev38
:precondition (and (token 1233) (tracePointer ev38))
:effect (and (allowed) (not (token 1233)) (token 1234) (not (tracePointer ev38)) (tracePointer ev39))
)

(:action moveInTheModel#activityba
:precondition (token 1233)
:effect (and (not (allowed)) (not (token 1233)) (token 1234) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityca
:precondition (token 1257)
:effect (and (not (allowed)) (not (token 1257)) (token 1258) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityct
:precondition (token 1177)
:effect (and (not (allowed)) (not (token 1177)) (token 1160) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycn
:precondition (token 1160)
:effect (and (not (allowed)) (not (token 1160)) (token 1208) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityo
:precondition (token 1175)
:effect (and (not (allowed)) (not (token 1175)) (token 1152) (increase (total-cost) 1)
)
)

(:action moveSync#activityb#ev6
:precondition (and (token 1152) (tracePointer ev6))
:effect (and (allowed) (not (token 1152)) (token 1221) (not (tracePointer ev6)) (tracePointer ev7))
)

(:action moveInTheModel#activityb
:precondition (token 1152)
:effect (and (not (allowed)) (not (token 1152)) (token 1221) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv2
:precondition (and (token 1207) (token 1204) (token 1202))
:effect (and (not (allowed)) (not (token 1207)) (not (token 1204)) (not (token 1202)) (token 1209) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activityy
:precondition (token 1217)
:effect (and (not (allowed)) (not (token 1217)) (token 1184) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityat
:precondition (token 1227)
:effect (and (not (allowed)) (not (token 1227)) (token 1228) (increase (total-cost) 1)
)
)

(:action moveSync#activityaa#ev10
:precondition (and (token 1155) (tracePointer ev10))
:effect (and (allowed) (not (token 1155)) (token 1220) (not (tracePointer ev10)) (tracePointer ev11))
)

(:action moveInTheModel#activityaa
:precondition (token 1155)
:effect (and (not (allowed)) (not (token 1155)) (token 1220) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybb
:precondition (token 1157)
:effect (and (not (allowed)) (not (token 1157)) (token 1237) (increase (total-cost) 1)
)
)

(:action moveSync#activityae#ev12
:precondition (and (token 1219) (tracePointer ev12))
:effect (and (allowed) (not (token 1219)) (token 1184) (not (tracePointer ev12)) (tracePointer ev13))
)

(:action moveInTheModel#activityae
:precondition (token 1219)
:effect (and (not (allowed)) (not (token 1219)) (token 1184) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityh
:precondition (token 1150)
:effect (and (not (allowed)) (not (token 1150)) (token 1200) (increase (total-cost) 1)
)
)

(:action moveSync#activityaj#ev18
:precondition (and (token 1168) (tracePointer ev18))
:effect (and (allowed) (not (token 1168)) (token 1170) (not (tracePointer ev18)) (tracePointer ev19))
)

(:action moveInTheModel#activityaj
:precondition (token 1168)
:effect (and (not (allowed)) (not (token 1168)) (token 1170) (increase (total-cost) 1)
)
)

(:action moveSync#activitye#ev4
:precondition (and (token 1196) (tracePointer ev4))
:effect (and (allowed) (not (token 1196)) (token 1194) (not (tracePointer ev4)) (tracePointer ev5))
)

(:action moveInTheModel#activitye
:precondition (token 1196)
:effect (and (not (allowed)) (not (token 1196)) (token 1194) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityau
:precondition (token 1168)
:effect (and (not (allowed)) (not (token 1168)) (token 1169) (increase (total-cost) 1)
)
)

(:action moveSync#activityai#ev35
:precondition (and (token 1169) (tracePointer ev35))
:effect (and (allowed) (not (token 1169)) (token 1238) (not (tracePointer ev35)) (tracePointer ev36))
)

(:action moveInTheModel#activityai
:precondition (token 1169)
:effect (and (not (allowed)) (not (token 1169)) (token 1238) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybn
:precondition (token 1188)
:effect (and (not (allowed)) (not (token 1188)) (token 1251) (increase (total-cost) 1)
)
)

(:action moveSync#activitycf#ev20
:precondition (and (token 1186) (tracePointer ev20))
:effect (and (allowed) (not (token 1186)) (token 1198) (not (tracePointer ev20)) (tracePointer ev21))
)

(:action moveInTheModel#activitycf
:precondition (token 1186)
:effect (and (not (allowed)) (not (token 1186)) (token 1198) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybh
:precondition (token 1180)
:effect (and (not (allowed)) (not (token 1180)) (token 1181) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityas
:precondition (token 1225)
:effect (and (not (allowed)) (not (token 1225)) (token 1226) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv3
:precondition (token 1213)
:effect (and (not (allowed)) (not (token 1213)) (token 1239) (token 1211) (token 1264) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activitybs
:precondition (token 1249)
:effect (and (not (allowed)) (not (token 1249)) (token 1250) (increase (total-cost) 1)
)
)

(:action moveSync#activityaw#ev41
:precondition (and (token 1157) (tracePointer ev41))
:effect (and (allowed) (not (token 1157)) (token 1240) (not (tracePointer ev41)) (tracePointer ev42))
)

(:action moveInTheModel#activityaw
:precondition (token 1157)
:effect (and (not (allowed)) (not (token 1157)) (token 1240) (increase (total-cost) 1)
)
)

(:action moveSync#activityaz#ev39
:precondition (and (token 1231) (tracePointer ev39))
:effect (and (allowed) (not (token 1231)) (token 1232) (not (tracePointer ev39)) (tracePointer ev40))
)

(:action moveInTheModel#activityaz
:precondition (token 1231)
:effect (and (not (allowed)) (not (token 1231)) (token 1232) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityj
:precondition (token 1206)
:effect (and (not (allowed)) (not (token 1206)) (token 1162) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityco
:precondition (token 1209)
:effect (and (not (allowed)) (not (token 1209)) (token 1161) (increase (total-cost) 1)
)
)

(:action moveSync#activityad#ev11
:precondition (and (token 1220) (tracePointer ev11))
:effect (and (allowed) (not (token 1220)) (token 1219) (not (tracePointer ev11)) (tracePointer ev12))
)

(:action moveInTheModel#activityad
:precondition (token 1220)
:effect (and (not (allowed)) (not (token 1220)) (token 1219) (increase (total-cost) 1)
)
)

(:action moveSync#activityag#ev42
:precondition (and (token 1214) (tracePointer ev42))
:effect (and (allowed) (not (token 1214)) (token 1046) (not (tracePointer ev42)) (tracePointer evEND))
)

(:action moveInTheModel#activityag
:precondition (token 1214)
:effect (and (not (allowed)) (not (token 1214)) (token 1046) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybw
:precondition (token 1254)
:effect (and (not (allowed)) (not (token 1254)) (token 1147) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityu
:precondition (token 1182)
:effect (and (not (allowed)) (not (token 1182)) (token 1184) (increase (total-cost) 1)
)
)

(:action moveSync#activityal#ev21
:precondition (and (token 1170) (tracePointer ev21))
:effect (and (allowed) (not (token 1170)) (token 1172) (not (tracePointer ev21)) (tracePointer ev22))
)

(:action moveSync#activityal#ev29
:precondition (and (token 1170) (tracePointer ev29))
:effect (and (allowed) (not (token 1170)) (token 1172) (not (tracePointer ev29)) (tracePointer ev30))
)

(:action moveInTheModel#activityal
:precondition (token 1170)
:effect (and (not (allowed)) (not (token 1170)) (token 1172) (increase (total-cost) 1)
)
)

(:action moveSync#activitya#ev1
:precondition (and (token 1045) (tracePointer ev1))
:effect (and (allowed) (not (token 1045)) (token 1150) (not (tracePointer ev1)) (tracePointer ev2))
)

(:action moveInTheModel#activitya
:precondition (token 1045)
:effect (and (not (allowed)) (not (token 1045)) (token 1150) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycb
:precondition (token 1260)
:effect (and (not (allowed)) (not (token 1260)) (token 1259) (increase (total-cost) 1)
)
)

(:action moveSync#activityf#ev5
:precondition (and (token 1194) (tracePointer ev5))
:effect (and (allowed) (not (token 1194)) (token 1152) (not (tracePointer ev5)) (tracePointer ev6))
)

(:action moveInTheModel#activityf
:precondition (token 1194)
:effect (and (not (allowed)) (not (token 1194)) (token 1152) (increase (total-cost) 1)
)
)

(:action moveSync#activitybe#ev33
:precondition (and (token 1179) (tracePointer ev33))
:effect (and (allowed) (not (token 1179)) (token 1265) (not (tracePointer ev33)) (tracePointer ev34))
)

(:action moveInTheModel#activitybe
:precondition (token 1179)
:effect (and (not (allowed)) (not (token 1179)) (token 1265) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv4
:precondition (and (token 1240) (token 1212) (token 1265))
:effect (and (not (allowed)) (not (token 1240)) (not (token 1212)) (not (token 1265)) (token 1214) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activityx
:precondition (token 1218)
:effect (and (not (allowed)) (not (token 1218)) (token 1217) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycm
:precondition (token 1161)
:effect (and (not (allowed)) (not (token 1161)) (token 1187) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybg
:precondition (token 1181)
:effect (and (not (allowed)) (not (token 1181)) (token 1242) (increase (total-cost) 1)
)
)

(:action moveSync#activityce#ev34
:precondition (and (token 1187) (tracePointer ev34))
:effect (and (allowed) (not (token 1187)) (token 1212) (not (tracePointer ev34)) (tracePointer ev35))
)

(:action moveInTheModel#activityce
:precondition (token 1187)
:effect (and (not (allowed)) (not (token 1187)) (token 1212) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybr
:precondition (token 1247)
:effect (and (not (allowed)) (not (token 1247)) (token 1248) (increase (total-cost) 1)
)
)

(:action moveSync#activitys#ev7
:precondition (and (token 1221) (tracePointer ev7))
:effect (and (allowed) (not (token 1221)) (token 1182) (not (tracePointer ev7)) (tracePointer ev8))
)

(:action moveInTheModel#activitys
:precondition (token 1221)
:effect (and (not (allowed)) (not (token 1221)) (token 1182) (increase (total-cost) 1)
)
)

(:action moveSync#activitych#ev25
:precondition (and (token 1190) (tracePointer ev25))
:effect (and (allowed) (not (token 1190)) (token 1189) (not (tracePointer ev25)) (tracePointer ev26))
)

(:action moveInTheModel#activitych
:precondition (token 1190)
:effect (and (not (allowed)) (not (token 1190)) (token 1189) (increase (total-cost) 1)
)
)

(:action moveSync#activitybt#ev22
:precondition (and (token 1188) (tracePointer ev22))
:effect (and (allowed) (not (token 1188)) (token 1253) (not (tracePointer ev22)) (tracePointer ev23))
)

(:action moveInTheModel#activitybt
:precondition (token 1188)
:effect (and (not (allowed)) (not (token 1188)) (token 1253) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybq
:precondition (token 1245)
:effect (and (not (allowed)) (not (token 1245)) (token 1246) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityp
:precondition (token 1171)
:effect (and (not (allowed)) (not (token 1171)) (token 1210) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv5
:precondition (token 1174)
:effect (and (not (allowed)) (not (token 1174)) (token 1177) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activityk
:precondition (token 1164)
:effect (and (not (allowed)) (not (token 1164)) (token 1152) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycs
:precondition (token 1161)
:effect (and (not (allowed)) (not (token 1161)) (token 1174) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybo
:precondition (token 1252)
:effect (and (not (allowed)) (not (token 1252)) (token 1147) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv6
:precondition (and (token 1234) (token 1232))
:effect (and (not (allowed)) (not (token 1234)) (not (token 1232)) (token 1236) (increase (total-cost) 0)
)
)

(:action moveSync#activityav#ev36
:precondition (and (token 1238) (tracePointer ev36))
:effect (and (allowed) (not (token 1238)) (token 1156) (not (tracePointer ev36)) (tracePointer ev37))
)

(:action moveInTheModel#activityav
:precondition (token 1238)
:effect (and (not (allowed)) (not (token 1238)) (token 1156) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv7
:precondition (token 1235)
:effect (and (not (allowed)) (not (token 1235)) (token 1231) (token 1233) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activityi
:precondition (token 1200)
:effect (and (not (allowed)) (not (token 1200)) (token 1206) (increase (total-cost) 1)
)
)

(:action moveSync#activityt#ev13
:precondition (and (token 1184) (tracePointer ev13))
:effect (and (allowed) (not (token 1184)) (token 1216) (not (tracePointer ev13)) (tracePointer ev14))
)

(:action moveInTheModel#activityt
:precondition (token 1184)
:effect (and (not (allowed)) (not (token 1184)) (token 1216) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybz
:precondition (token 1255)
:effect (and (not (allowed)) (not (token 1255)) (token 1256) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybp
:precondition (token 1243)
:effect (and (not (allowed)) (not (token 1243)) (token 1244) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityby
:precondition (token 1263)
:effect (and (not (allowed)) (not (token 1263)) (token 1179) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityv
:precondition (token 1182)
:effect (and (not (allowed)) (not (token 1182)) (token 1215) (increase (total-cost) 1)
)
)

(:action moveSync#activitybd#ev16
:precondition (and (token 1264) (tracePointer ev16))
:effect (and (allowed) (not (token 1264)) (token 1178) (not (tracePointer ev16)) (tracePointer ev17))
)

(:action moveInTheModel#activitybd
:precondition (token 1264)
:effect (and (not (allowed)) (not (token 1264)) (token 1178) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityap
:precondition (token 1168)
:effect (and (not (allowed)) (not (token 1168)) (token 1229) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv8
:precondition (token 1208)
:effect (and (not (allowed)) (not (token 1208)) (token 1201) (token 1205) (token 1203) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activitycu
:precondition (token 1174)
:effect (and (not (allowed)) (not (token 1174)) (token 1177) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybv
:precondition (token 1188)
:effect (and (not (allowed)) (not (token 1188)) (token 1254) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityr
:precondition (token 1175)
:effect (and (not (allowed)) (not (token 1175)) (token 1171) (increase (total-cost) 1)
)
)

(:action moveSync#activityax#ev37
:precondition (and (token 1156) (tracePointer ev37))
:effect (and (allowed) (not (token 1156)) (token 1235) (not (tracePointer ev37)) (tracePointer ev38))
)

(:action moveInTheModel#activityax
:precondition (token 1156)
:effect (and (not (allowed)) (not (token 1156)) (token 1235) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv9
:precondition (and (token 1197) (token 1191))
:effect (and (not (allowed)) (not (token 1197)) (not (token 1191)) (token 1199) (increase (total-cost) 0)
)
)

(:action moveInTheModel#generatedinv10
:precondition (token 1229)
:effect (and (not (allowed)) (not (token 1229)) (token 1225) (token 1227) (token 1223) (increase (total-cost) 0)
)
)

(:action moveInTheModel#generatedinv11
:precondition (token 1251)
:effect (and (not (allowed)) (not (token 1251)) (token 1247) (token 1249) (token 1243) (token 1245) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activitybk
:precondition (token 1241)
:effect (and (not (allowed)) (not (token 1241)) (token 1179) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybc
:precondition (token 1237)
:effect (and (not (allowed)) (not (token 1237)) (token 1156) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityao
:precondition (token 1222)
:effect (and (not (allowed)) (not (token 1222)) (token 1169) (increase (total-cost) 1)
)
)

(:action moveSync#activityck#ev27
:precondition (and (token 1193) (tracePointer ev27))
:effect (and (allowed) (not (token 1193)) (token 1197) (not (tracePointer ev27)) (tracePointer ev28))
)

(:action moveInTheModel#activityck
:precondition (token 1193)
:effect (and (not (allowed)) (not (token 1193)) (token 1197) (increase (total-cost) 1)
)
)

(:action moveSync#activityaf#ev14
:precondition (and (token 1216) (tracePointer ev14))
:effect (and (allowed) (not (token 1216)) (token 1213) (not (tracePointer ev14)) (tracePointer ev15))
)

(:action moveInTheModel#activityaf
:precondition (token 1216)
:effect (and (not (allowed)) (not (token 1216)) (token 1213) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityaq
:precondition (token 1230)
:effect (and (not (allowed)) (not (token 1230)) (token 1169) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycv
:precondition (token 1174)
:effect (and (not (allowed)) (not (token 1174)) (token 1177) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv12
:precondition (and (token 1250) (token 1244) (token 1246) (token 1248))
:effect (and (not (allowed)) (not (token 1250)) (not (token 1244)) (not (token 1246)) (not (token 1248)) (token 1252) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activityar
:precondition (token 1223)
:effect (and (not (allowed)) (not (token 1223)) (token 1224) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybx
:precondition (token 1178)
:effect (and (not (allowed)) (not (token 1178)) (token 1262) (increase (total-cost) 1)
)
)

(:action moveSync#activityz#ev8
:precondition (and (token 1182) (tracePointer ev8))
:effect (and (allowed) (not (token 1182)) (token 1154) (not (tracePointer ev8)) (tracePointer ev9))
)

(:action moveInTheModel#activityz
:precondition (token 1182)
:effect (and (not (allowed)) (not (token 1182)) (token 1154) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv13
:precondition (token 1262)
:effect (and (not (allowed)) (not (token 1262)) (token 1257) (token 1260) (token 1255) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activitycp
:precondition (token 1201)
:effect (and (not (allowed)) (not (token 1201)) (token 1202) (increase (total-cost) 1)
)
)

(:action moveSync#activitybu#ev26
:precondition (and (token 1253) (tracePointer ev26))
:effect (and (allowed) (not (token 1253)) (token 1147) (not (tracePointer ev26)) (tracePointer ev27))
)

(:action moveInTheModel#activitybu
:precondition (token 1253)
:effect (and (not (allowed)) (not (token 1253)) (token 1147) (increase (total-cost) 1)
)
)

(:action moveSync#activityd#ev2
:precondition (and (token 1192) (tracePointer ev2))
:effect (and (allowed) (not (token 1192)) (token 1196) (not (tracePointer ev2)) (tracePointer ev3))
)

(:action moveInTheModel#activityd
:precondition (token 1192)
:effect (and (not (allowed)) (not (token 1192)) (token 1196) (increase (total-cost) 1)
)
)

(:action moveSync#activitycj#ev23
:precondition (and (token 1195) (tracePointer ev23))
:effect (and (allowed) (not (token 1195)) (token 1193) (not (tracePointer ev23)) (tracePointer ev24))
)

(:action moveInTheModel#activitycj
:precondition (token 1195)
:effect (and (not (allowed)) (not (token 1195)) (token 1193) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitycc
:precondition (token 1259)
:effect (and (not (allowed)) (not (token 1259)) (token 1261) (increase (total-cost) 1)
)
)

(:action moveSync#activityam#ev24
:precondition (and (token 1172) (tracePointer ev24))
:effect (and (allowed) (not (token 1172)) (token 1170) (not (tracePointer ev24)) (tracePointer ev25))
)

(:action moveInTheModel#activityam
:precondition (token 1172)
:effect (and (not (allowed)) (not (token 1172)) (token 1170) (increase (total-cost) 1)
)
)

(:action moveInTheModel#generatedinv14
:precondition (token 1198)
:effect (and (not (allowed)) (not (token 1198)) (token 1195) (token 1190) (increase (total-cost) 0)
)
)

(:action moveInTheModel#activityl
:precondition (token 1162)
:effect (and (not (allowed)) (not (token 1162)) (token 1164) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitym
:precondition (token 1162)
:effect (and (not (allowed)) (not (token 1162)) (token 1164) (increase (total-cost) 1)
)
)

(:action moveSync#activitybm#ev30
:precondition (and (token 1147) (tracePointer ev30))
:effect (and (allowed) (not (token 1147)) (token 1179) (not (tracePointer ev30)) (tracePointer ev31))
)

(:action moveInTheModel#activitybm
:precondition (token 1147)
:effect (and (not (allowed)) (not (token 1147)) (token 1179) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activitybi
:precondition (token 1181)
:effect (and (not (allowed)) (not (token 1181)) (token 1180) (increase (total-cost) 1)
)
)

(:action moveInTheModel#activityn
:precondition (token 1150)
:effect (and (not (allowed)) (not (token 1150)) (token 1171) (increase (total-cost) 1)
)
)

(:action moveInTheLog#activitya#ev1-ev2
:precondition (and (tracePointer ev1) (allowed))
:effect (and (not (tracePointer ev1)) (tracePointer ev2) (increase (total-cost) 1)
))

(:action moveInTheLog#activityd#ev2-ev3
:precondition (and (tracePointer ev2) (allowed))
:effect (and (not (tracePointer ev2)) (tracePointer ev3) (increase (total-cost) 1)
))

(:action moveInTheLog#activityc#ev3-ev4
:precondition (and (tracePointer ev3) (allowed))
:effect (and (not (tracePointer ev3)) (tracePointer ev4) (increase (total-cost) 1)
))

(:action moveInTheLog#activitye#ev4-ev5
:precondition (and (tracePointer ev4) (allowed))
:effect (and (not (tracePointer ev4)) (tracePointer ev5) (increase (total-cost) 1)
))

(:action moveInTheLog#activityf#ev5-ev6
:precondition (and (tracePointer ev5) (allowed))
:effect (and (not (tracePointer ev5)) (tracePointer ev6) (increase (total-cost) 1)
))

(:action moveInTheLog#activityb#ev6-ev7
:precondition (and (tracePointer ev6) (allowed))
:effect (and (not (tracePointer ev6)) (tracePointer ev7) (increase (total-cost) 1)
))

(:action moveInTheLog#activitys#ev7-ev8
:precondition (and (tracePointer ev7) (allowed))
:effect (and (not (tracePointer ev7)) (tracePointer ev8) (increase (total-cost) 1)
))

(:action moveInTheLog#activityz#ev8-ev9
:precondition (and (tracePointer ev8) (allowed))
:effect (and (not (tracePointer ev8)) (tracePointer ev9) (increase (total-cost) 1)
))

(:action moveInTheLog#activityab#ev9-ev10
:precondition (and (tracePointer ev9) (allowed))
:effect (and (not (tracePointer ev9)) (tracePointer ev10) (increase (total-cost) 1)
))

(:action moveInTheLog#activityaa#ev10-ev11
:precondition (and (tracePointer ev10) (allowed))
:effect (and (not (tracePointer ev10)) (tracePointer ev11) (increase (total-cost) 1)
))

(:action moveInTheLog#activityad#ev11-ev12
:precondition (and (tracePointer ev11) (allowed))
:effect (and (not (tracePointer ev11)) (tracePointer ev12) (increase (total-cost) 1)
))

(:action moveInTheLog#activityae#ev12-ev13
:precondition (and (tracePointer ev12) (allowed))
:effect (and (not (tracePointer ev12)) (tracePointer ev13) (increase (total-cost) 1)
))

(:action moveInTheLog#activityt#ev13-ev14
:precondition (and (tracePointer ev13) (allowed))
:effect (and (not (tracePointer ev13)) (tracePointer ev14) (increase (total-cost) 1)
))

(:action moveInTheLog#activityaf#ev14-ev15
:precondition (and (tracePointer ev14) (allowed))
:effect (and (not (tracePointer ev14)) (tracePointer ev15) (increase (total-cost) 1)
))

(:action moveInTheLog#activityah#ev15-ev16
:precondition (and (tracePointer ev15) (allowed))
:effect (and (not (tracePointer ev15)) (tracePointer ev16) (increase (total-cost) 1)
))

(:action moveInTheLog#activitybd#ev16-ev17
:precondition (and (tracePointer ev16) (allowed))
:effect (and (not (tracePointer ev16)) (tracePointer ev17) (increase (total-cost) 1)
))

(:action moveInTheLog#activitycd#ev17-ev18
:precondition (and (tracePointer ev17) (allowed))
:effect (and (not (tracePointer ev17)) (tracePointer ev18) (increase (total-cost) 1)
))

(:action moveInTheLog#activityaj#ev18-ev19
:precondition (and (tracePointer ev18) (allowed))
:effect (and (not (tracePointer ev18)) (tracePointer ev19) (increase (total-cost) 1)
))

(:action moveInTheLog#activitybl#ev19-ev20
:precondition (and (tracePointer ev19) (allowed))
:effect (and (not (tracePointer ev19)) (tracePointer ev20) (increase (total-cost) 1)
))

(:action moveInTheLog#activitycf#ev20-ev21
:precondition (and (tracePointer ev20) (allowed))
:effect (and (not (tracePointer ev20)) (tracePointer ev21) (increase (total-cost) 1)
))

(:action moveInTheLog#activityal#ev21-ev22
:precondition (and (tracePointer ev21) (allowed))
:effect (and (not (tracePointer ev21)) (tracePointer ev22) (increase (total-cost) 1)
))

(:action moveInTheLog#activitybt#ev22-ev23
:precondition (and (tracePointer ev22) (allowed))
:effect (and (not (tracePointer ev22)) (tracePointer ev23) (increase (total-cost) 1)
))

(:action moveInTheLog#activitycj#ev23-ev24
:precondition (and (tracePointer ev23) (allowed))
:effect (and (not (tracePointer ev23)) (tracePointer ev24) (increase (total-cost) 1)
))

(:action moveInTheLog#activityam#ev24-ev25
:precondition (and (tracePointer ev24) (allowed))
:effect (and (not (tracePointer ev24)) (tracePointer ev25) (increase (total-cost) 1)
))

(:action moveInTheLog#activitych#ev25-ev26
:precondition (and (tracePointer ev25) (allowed))
:effect (and (not (tracePointer ev25)) (tracePointer ev26) (increase (total-cost) 1)
))

(:action moveInTheLog#activitybu#ev26-ev27
:precondition (and (tracePointer ev26) (allowed))
:effect (and (not (tracePointer ev26)) (tracePointer ev27) (increase (total-cost) 1)
))

(:action moveInTheLog#activityck#ev27-ev28
:precondition (and (tracePointer ev27) (allowed))
:effect (and (not (tracePointer ev27)) (tracePointer ev28) (increase (total-cost) 1)
))

(:action moveInTheLog#activityci#ev28-ev29
:precondition (and (tracePointer ev28) (allowed))
:effect (and (not (tracePointer ev28)) (tracePointer ev29) (increase (total-cost) 1)
))

(:action moveInTheLog#activityal#ev29-ev30
:precondition (and (tracePointer ev29) (allowed))
:effect (and (not (tracePointer ev29)) (tracePointer ev30) (increase (total-cost) 1)
))

(:action moveInTheLog#activitybm#ev30-ev31
:precondition (and (tracePointer ev30) (allowed))
:effect (and (not (tracePointer ev30)) (tracePointer ev31) (increase (total-cost) 1)
))

(:action moveInTheLog#activitycg#ev31-ev32
:precondition (and (tracePointer ev31) (allowed))
:effect (and (not (tracePointer ev31)) (tracePointer ev32) (increase (total-cost) 1)
))

(:action moveInTheLog#activityak#ev32-ev33
:precondition (and (tracePointer ev32) (allowed))
:effect (and (not (tracePointer ev32)) (tracePointer ev33) (increase (total-cost) 1)
))

(:action moveInTheLog#activitybe#ev33-ev34
:precondition (and (tracePointer ev33) (allowed))
:effect (and (not (tracePointer ev33)) (tracePointer ev34) (increase (total-cost) 1)
))

(:action moveInTheLog#activityce#ev34-ev35
:precondition (and (tracePointer ev34) (allowed))
:effect (and (not (tracePointer ev34)) (tracePointer ev35) (increase (total-cost) 1)
))

(:action moveInTheLog#activityai#ev35-ev36
:precondition (and (tracePointer ev35) (allowed))
:effect (and (not (tracePointer ev35)) (tracePointer ev36) (increase (total-cost) 1)
))

(:action moveInTheLog#activityav#ev36-ev37
:precondition (and (tracePointer ev36) (allowed))
:effect (and (not (tracePointer ev36)) (tracePointer ev37) (increase (total-cost) 1)
))

(:action moveInTheLog#activityax#ev37-ev38
:precondition (and (tracePointer ev37) (allowed))
:effect (and (not (tracePointer ev37)) (tracePointer ev38) (increase (total-cost) 1)
))

(:action moveInTheLog#activityba#ev38-ev39
:precondition (and (tracePointer ev38) (allowed))
:effect (and (not (tracePointer ev38)) (tracePointer ev39) (increase (total-cost) 1)
))

(:action moveInTheLog#activityaz#ev39-ev40
:precondition (and (tracePointer ev39) (allowed))
:effect (and (not (tracePointer ev39)) (tracePointer ev40) (increase (total-cost) 1)
))

(:action moveInTheLog#activityay#ev40-ev41
:precondition (and (tracePointer ev40) (allowed))
:effect (and (not (tracePointer ev40)) (tracePointer ev41) (increase (total-cost) 1)
))

(:action moveInTheLog#activityaw#ev41-ev42
:precondition (and (tracePointer ev41) (allowed))
:effect (and (not (tracePointer ev41)) (tracePointer ev42) (increase (total-cost) 1)
))

(:action moveInTheLog#activityag#ev42-evEND
:precondition (and (tracePointer ev42) (allowed))
:effect (and (not (tracePointer ev42)) (tracePointer evEND) (increase (total-cost) 1)
))

)