import os


from_folder = "/home/alvaro/IPC18/benchmarks/generators/petri-net-alignment/plan-based-imperative-conformance-checker/PDDLfiles/"


for d in os.listdir(from_folder):
    if d.startswith("domain"):
        continue

    objects = []

    with open(from_folder + d) as problem:
            new_problem = open(d.replace("problem", "p"), "w")
    
            reading_objects = False
            for l in problem.readlines():
                if reading_objects:
                    if l.strip() == ")":
                        reading_objects = False
                    else:
                        if "place" in l:
                            objects.append("t"+ l)
                        else:
                            objects.append(l)
                else:
                    if l.strip() == "(:objects":
                        reading_objects = True
                    else: 
                        new_problem.write(l.replace("(token ", "(token t"))
                
            new_problem.close()

            

    with open(from_folder + d.replace("problem", "domain")) as domain:
        new_domain = open(d.replace("problem", "domain-p"), "w")

        for l in domain.readlines():
            if ":precondition" in l:
                new_domain.write(":parameters ()\n")
            if l.strip() == "(:predicates":
                new_domain.write("(:constants\n")
                for o in objects:
                    new_domain.write(o)
                new_domain.write("\n)\n")
            new_domain.write(l.replace("(token ", "(token t").replace("t?", "?").replace("#", "-"))
        new_domain.close()
    
