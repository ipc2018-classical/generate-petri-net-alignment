#!/usr/bin/env python2
import argparse
import datetime
import random

random.seed(2018)
parser = argparse.ArgumentParser()
parser.add_argument("traces", help="What kind of board.")

args = parser.parse_args()    

with open(args.traces) as f:
    traces = []
    current_trace = []
    for l in f.readlines():
        if "</trace>" in l:
            traces.append(current_trace)
            current_trace = []
        if '<string key="concept:name' in l and 'Activity' in l:
           current_trace.append(l.strip().split("value=")[1].split('"')[1])


def print_trace (ids, trace):
    time = datetime.datetime(1970, 01, 01) + datetime.timedelta(hours=1)
    print("<trace>")
    print ('<string key="concept:name" value="case_{}"/>'.format(ids))
	
    for event in trace:
        print("""	<event>
        <string key="concept:name" value="{}"/>
        <date key="time:timestamp" value="{}"/>
        </event>
        """.format(event, str(time).replace(" ", "T") + "+01:00"))
        time += datetime.timedelta(hours=1)
    print("</trace>")

def add_noise (trace, noise, traces):
    second_trace = random.choice(traces)
    
    if noise > len(second_trace):
        second_trace += random.choice(traces)
    new_activities = random.sample(second_trace, noise)
    
    new_trace = []
    i = 0
    j = 0
    while len(new_trace) < len(trace) + len(new_activities):
        if i == len(trace):
            return new_trace + new_activities[j:]
        if j == len(new_activities):
            return new_trace + trace[i:]
        
        if random.choice ([True, False]):
            new_trace.append(trace[i])
            i += 1
        else:
            new_trace.append(new_activities[j])
            j += 1
    

def swap_noise (trace, noise):
    for i in range(0, len(trace)-1):
        if random.randint(0, 100) < noise:
            tmp = trace[i]
            trace [i] = trace[i+1]
            trace[i+1] = tmp

    return trace


print ("""<?xml version="1.0" encoding="UTF-8" ?>
<!-- This file has been generated with the OpenXES library. It conforms -->
<!-- to the XML serialization of the XES standard for log storage and -->
<!-- management. -->
<!-- XES standard version: 1.0 -->
<!-- OpenXES library version: 1.0RC7 -->
<!-- OpenXES is available from http://www.openxes.org/ -->
<log xes.version="1.0" xes.features="nested-attributes" openxes.version="1.0RC7">
	<string key="concept:name" value="XLog30"/>
""")
ids = 0
noise_swap = 30
for noise_added in range (0, 200, 10):
    new_trace = random.choice (traces)
    if noise_added:
        new_trace = add_noise(traces[0], noise_added, traces)
    new_trace = swap_noise(new_trace, noise_swap)
    print_trace(ids, new_trace)
    ids += 1
print ("</log>")



