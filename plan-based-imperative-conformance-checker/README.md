# Planning-based Imperative Conformance Checker

Before running the tool:
1. Download the pre-built [Fast-Downward planner](https://drive.google.com/file/d/0B0FHkIijDk2heXd1X2FaUmktRDQ/view?usp=sharing).
2. Unpack the .zip archive in the root folder of the Eclipse project.
3. For **Windows users** only:
	1. Download [standalone Python 2.7 interpreter](https://drive.google.com/file/d/0B0FHkIijDk2haVRxVE9ibzJXN00/view?usp=sharing).
	2. Unpack the .zip archive in the root folder of the Eclipse project.
4. For **Linux/Mac users** only: make sure that Python 2.7 interpreter is set as default.
